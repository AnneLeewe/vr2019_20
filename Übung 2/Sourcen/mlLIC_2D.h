//----------------------------------------------------------------------------------
//! The ML module class LIC_2D.
/*!
// \file    
// \author  Ehricke
// \date    2013-08-07
//
// 2D Line Integral Convolution
*/
//----------------------------------------------------------------------------------


#ifndef __mlLIC_2D_H
#define __mlLIC_2D_H


// Local includes
#include "_MLLIC_2DSystem.h"

// ML includes
#include <mlModuleIncludes.h>

ML_START_NAMESPACE


//! 2D Line Integral Convolution
class _MLLIC_2D_EXPORT LIC_2D : public Module
{
public:

  //! Constructor.
  LIC_2D();

  //! Handles field changes of the field \p field.
  virtual void handleNotification (Field* field);

  //! Updates the internal module state after loading or cloning the module,
  //! and enables notification handling again.
  virtual void activateAttachments();

  // ----------------------------------------------------------
  //! \name Image processing methods.
  //@{
  // ----------------------------------------------------------

  //! Sets properties of the output image at output \p outputIndex.
  virtual void calculateOutputImageProperties(int outputIndex, PagedImage* outputImage);

  //! Returns the input image region required to calculate a region of an output image.
  //! \param inputIndex        The input of which the regions shall be calculated.
  //! \param outputSubImageBox The region of the output image for which the required input region
  //!                          shall be calculated.
  //! \param outputIndex       The index of the output image for which the required input region
  //!                          shall be calculated.
  //! \return Region of input image needed to compute the region \p outputSubImageBox on output \p outputIndex.
  virtual SubImageBox calculateInputSubImageBox(int inputIndex, const SubImageBox& outputSubImageBox, int outputIndex);

  //! Calculates page \p outputSubImage of output image with index \p outputIndex by using \p inputSubImages.
  //! \param outputSubImage The sub-image of output image \p outputIndex calculated from \p inputSubImges.
  //! \param outputIndex    The index of the output the sub-image is calculated for.
  //! \param inputSubImages Array of sub-image(s) of the input(s) whose extents were specified
  //!                       by calculateInputSubImageBox. Array size is given by getNumInputImages().
  virtual void calculateOutputSubImage(SubImage* outputSubImage, int outputIndex, SubImage* inputSubImages);

  //! Method template for type-specific page calculation. Called by calculateOutputSubImage().
  //! \param outputSubImage The typed sub-image of output image \p outputIndex calculated from \p inputSubImages.
  //! \param outputIndex    The index of the output the sub-image is calculated for.
  //! \param inSubImg0 Temporary typed sub-image of input 0.
  //! \param inSubImg1 Temporary typed sub-image of input 1.
  //! \param inSubImg2 Temporary typed sub-image of input 2.
  template <typename T>
  void calculateOutputSubImage (TSubImage<T>* outputSubImage, int outputIndex
                               , TSubImage<T>* inputSubImage0
                               , TSubImage<T>* inputSubImage1
                               , TSubImage<T>* inputSubImage2
                               );
  template <typename T>
  MLdouble LIC(ImageVector p, SubImageBox validOutBox, TSubImage<T>* inputSubImage0
                                     , TSubImage<T>* inputSubImage1
                                     , TSubImage<T>* inputSubImage2
									 , int *noise_image
									 , int xsize);
  //@}

private:

  // ----------------------------------------------------------
  //! \name Module field declarations
  //@{
  // ----------------------------------------------------------

  //! Length of integration step
  FloatField* _StepsizeFld;
  //! Number of integration steps (single direction)
  IntField* _KernellengthFld;
  //@}

  // Implements interface for the runtime type system of the ML.
  ML_MODULE_CLASS_HEADER(LIC_2D)
};


ML_END_NAMESPACE

#endif // __mlLIC_2D_H