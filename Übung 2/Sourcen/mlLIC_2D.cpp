//----------------------------------------------------------------------------------
//! The ML module class LIC_2D.
/*!
// \file   
// \author  Ehricke
// \date    2013-08-07
//
// 2D Line Integral Convolution
*/
//----------------------------------------------------------------------------------

// Local includes
#include "mlLIC_2D.h"


ML_START_NAMESPACE

//! Implements code for the runtime type system of the ML
ML_MODULE_CLASS_SOURCE(LIC_2D, Module);

//----------------------------------------------------------------------------------

LIC_2D::LIC_2D() : Module(3, 1)
{
  // Suppress calls of handleNotification on field changes to
  // avoid side effects during initialization phase.
  handleNotificationOff();

  // Add fields to the module and set their values.
  _StepsizeFld = addFloat("Stepsize", 0.5);
  _KernellengthFld = addInt("Kernellength", 10);

  // Reactivate calls of handleNotification on field changes.
  handleNotificationOn();

}

//----------------------------------------------------------------------------------

void LIC_2D::handleNotification(Field* field)
{
  // Handle changes of module parameters and input image fields here.
  bool touchOutputs = false;
  if (isInputImageField(field))
  {
    touchOutputs = true;
  }
  else if (field == _StepsizeFld)
  {
    touchOutputs = true;
  }
  else if (field == _KernellengthFld)
  {
    touchOutputs = true;
  }

  if (touchOutputs) 
  {
    // Touch all output image fields to notify connected modules.
    touchOutputImageFields();
  }
}

//----------------------------------------------------------------------------------

void LIC_2D::activateAttachments()
{
  // Update members to new field state here.
  // Call super class functionality to enable notification handling again.
  Module::activateAttachments();
}

//----------------------------------------------------------------------------------

void LIC_2D::calculateOutputImageProperties(int /*outputIndex*/, PagedImage* outputImage)
{
  // Change properties of output image outputImage here whose
  // defaults are inherited from the input image 0 (if there is one).

  // Set output image to a fixed type.
  outputImage->setDataType(MLdoubleType);

  outputImage->setMaxVoxelValue(1000.0);
  outputImage->setMinVoxelValue(0.0);


  // Specify input sub-image data types (otherwise the above output data type is used for all input sub-images).
  //outputImage->setInputSubImageDataType(0, getInputImage(0)->getDataType());
  //outputImage->setInputSubImageDataType(1, getInputImage(1)->getDataType());
  //outputImage->setInputSubImageDataType(2, getInputImage(2)->getDataType());
}

//----------------------------------------------------------------------------------

SubImageBox LIC_2D::calculateInputSubImageBox(int inputIndex, const SubImageBox& outputSubImageBox, int outputIndex)
{
  // Return region of input image inputIndex needed to compute region
  // outSubImgBox of output image outputIndex.
  return outputSubImageBox;
}

//----------------------------------------------------------------------------------
template <typename T>
MLdouble LIC_2D::LIC(ImageVector p, SubImageBox validOutBox, TSubImage<T>* inputSubImage0
                                     , TSubImage<T>* inputSubImage1
                                     , TSubImage<T>* inputSubImage2
									 , int *noise_image
									 , int xsize)
// p: Ortskoordinaten des Startpunktes
// validOutBox: Eckpunkte der Region of Interest, in der die LIC Filterung durchgef�hrt werden soll
// inputSubImage0: Zeiger auf Eingabebild 0
// inputSubImage1: Zeiger auf Eingabebild 1
// inputSubImage2: Zeiger auf Eingabebild 2
// noise_image: Zeiger auf das erste Pixel des 2-dim. Rauschbildes
// xsize: Pixelbreite des zweidimensionalen Rauschbildes
{
	// Zugriff auf die Komponenten des Richtungsvektors am Startpunkt
	T* inVoxel0 = inputSubImage0->getImagePointer(p); //x-Richtung
	T* inVoxel1 = inputSubImage1->getImagePointer(p); //y-Richtung
	//T* inVoxel2 = inputSubImage2->getImagePointer(p); //z-Richtung (wird nicht gebraucht)

	// Skalierungsfaktor f�r Richtungsvektoren in der xy-Ebene auf 1
	MLdouble scale = 1.0/ sqrtf(*inVoxel0 * *inVoxel0 + *inVoxel1 * *inVoxel1);

	// hier beginnt die eigene Implementierung


	
	// Aufbau des Line-Kernels in Vektorrichtung
	for (int i=0; i<_KernellengthFld->getIntValue(); i++)
	{
		
	}

	


	// Aufbau des Line-Kernels in Vektorgegenrichtung
	for (int i=0; i<_KernellengthFld->getIntValue(); i++)
	{
	
	}

	
	return 0.0;
}




//----------------------------------------------------------------------------------

ML_CALCULATEOUTPUTSUBIMAGE_NUM_INPUTS_3_CPP(LIC_2D);

template <typename T>
void LIC_2D::calculateOutputSubImage(TSubImage<T>* outputSubImage, int outputIndex
                                     , TSubImage<T>* inputSubImage0
                                     , TSubImage<T>* inputSubImage1
                                     , TSubImage<T>* inputSubImage2
                                     )
{
  // Compute sub-image of output image outputIndex from input sub-images.

  // Clamp box of output image against image extent to avoid that unused areas are processed.
  const SubImageBox validOutBox = outputSubImage->getValidRegion();


  // Create noise image
  int xsize = abs(validOutBox.v2.x - validOutBox.v1.x);
  int ysize = abs(validOutBox.v2.y - validOutBox.v1.y);

  int *noise_image = (int *)malloc(xsize*ysize*sizeof(int));
  int *image_ptr = noise_image;

  for(int i = 0; i<xsize*ysize;i++)
	 *image_ptr++= rand() % 1000;


  // Process all voxels of the valid region of the output page.
  ImageVector p;
  for (p.u=validOutBox.v1.u;  p.u<=validOutBox.v2.u;  ++p.u) {
    for (p.t=validOutBox.v1.t;  p.t<=validOutBox.v2.t;  ++p.t) {
      for (p.c=validOutBox.v1.c;  p.c<=validOutBox.v2.c;  ++p.c) {
        for (p.z=validOutBox.v1.z;  p.z<=validOutBox.v2.z;  ++p.z) {
          for (p.y=validOutBox.v1.y;  p.y<=validOutBox.v2.y;  ++p.y) {

            p.x = validOutBox.v1.x;
            // Get pointers to row starts of input and output sub-images.
            const T* inVoxel0 = inputSubImage0->getImagePointer(p);

            T*  outVoxel = outputSubImage->getImagePointer(p);

            const MLint rowEnd   = validOutBox.v2.x;

            // Process all row voxels.
            for (; p.x <= rowEnd;  ++p.x, ++outVoxel, ++inVoxel0)
            {
				// call LIC method
				*outVoxel = LIC(p,validOutBox,inputSubImage0, inputSubImage1,inputSubImage2,noise_image,xsize);
            }
          }
        }
      }
    }
  }
  free(noise_image);
}

ML_END_NAMESPACE