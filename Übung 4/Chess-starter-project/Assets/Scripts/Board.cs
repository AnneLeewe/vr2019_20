﻿/*
 * Copyright (c) 2018 Razeware LLC
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * Notwithstanding the foregoing, you may not use, copy, modify, merge, publish, 
 * distribute, sublicense, create a derivative work, and/or sell copies of the 
 * Software in any work that is designed, intended, or marketed for pedagogical or 
 * instructional purposes related to programming, coding, application development, 
 * or information technology.  Permission for such use, copying, modification,
 * merger, publication, distribution, sublicensing, creation of derivative works, 
 * or sale is expressly withheld.
 *    
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

using UnityEngine;

public class Board : MonoBehaviour
{
    public Material defaultMaterial;
    public Material selectedMaterial;

    private GameObject movingPiece;
    private Vector3 endPosition;

    private GameObject pieceToCapture;

    public GameObject AddPiece(GameObject piece, int col, int row)
    {
        Vector2Int gridPoint = Geometry.GridPoint(col, row);
        GameObject newPiece = Instantiate(piece, Geometry.PointFromGrid(gridPoint),
            Quaternion.identity, gameObject.transform);
        return newPiece;
    }

    public void RemovePiece(GameObject piece)
    {
        pieceToCapture = piece;
    }

    

    public void MovePiece(GameObject piece, Vector2Int gridPoint)
    {
        // piece.transform.position = Geometry.PointFromGrid(gridPoint);

        movingPiece = piece;
        endPosition = Geometry.PointFromGrid(gridPoint);

        ConstantForce f = piece.gameObject.GetComponent<ConstantForce>();
        //determine direction for movement between current piece position and endPosition
        Vector3 forceDirection = endPosition - movingPiece.transform.position;
        f.force = Vector3.Normalize(forceDirection) * 50;

        
    }

   
    public void SelectPiece(GameObject piece)
    {
        MeshRenderer renderers = piece.GetComponentInChildren<MeshRenderer>();
        renderers.material = selectedMaterial;
    }

    public void DeselectPiece(GameObject piece)
    {
        MeshRenderer renderers = piece.GetComponentInChildren<MeshRenderer>();
        renderers.material = defaultMaterial;
    }

    public void Update()
    {
        //stop moving piece by setting force to 0
        if(movingPiece)
        {
            if ((movingPiece.transform.position - endPosition).magnitude < 0.1)
            {
                ConstantForce f = movingPiece.gameObject.GetComponent<ConstantForce>();
                f.force = new Vector3(0, 0, 0);
                movingPiece.gameObject.GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);

                movingPiece.gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
            }
        }
       
        
        if(pieceToCapture != null )
        {
            if(pieceToCapture.GetComponent<Rigidbody>().velocity.magnitude < 0.02f)
            {
                Destroy(pieceToCapture);
                pieceToCapture = null;
            }
        }



    }

}
